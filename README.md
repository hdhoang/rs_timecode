rs_timecode
===

[![Build Status](https://travis-ci.org/MarcAntoine-Arnaud/rs_timecode.svg?branch=master)](https://travis-ci.org/MarcAntoine-Arnaud/rs_timecode)


Library to parse coded timecode.

Supported formats:  
- [x] SMPTE 12M  
- [x] SMPTE 331M
- [ ] SMPTE 309M

=

Supported by [Nomalab](http://www.nomalab.com/).

<img src="http://www.nomalab.com/images/logo.svg" alt="Logo" height="30"/>
